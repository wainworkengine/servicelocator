# Module of WainWorkEngine

Dependent on:
[WainWorkEngine.Extensions] v1.0.0+

## Getting started

Add this to your Packages\manifest.json
```
"com.wainwork.engine.extensions": "https://gitlab.com/wainworkengine/extensions.git",
"com.wainwork.engine.servicelocator": "https://gitlab.com/wainworkengine/servicelocator.git",
```

## Описание
Это сервис для доступа к экземплярам других сервисов и/или классов.

Рекомендуется иметь к нему статичный доступ.



## Создание экземпляра локатора:
```
var locator = new SrviceLocator();
var locator = new SrviceLocator(new List<IServiceLocatorModule>{ new MyCustomModule() });
var locator = new SrviceLocator(new MyCustomModule(), new OtherCustomModule() );
var locator = new SrviceLocator(new MyCustomModuleController() );
```
Модуль для обработки IDisposible будет добавлен самостоятельно



## Добавить сервис:
```
ServiceLocator locator;
Service service; 

locator.Add(typeof(IServiceInterface), service, false);
or
locator.Add<IServiceInterface>(service, false);
```



## Добавить уникальный сервис:
```
ServiceLocator locator;
Service service; 

locator.Add(typeof(IServiceInterface), service, true);
or
locator.Add<IServiceInterface>(service, true);
```
При добавлении уникального сервиса, удаляются все сервисы с таким же типом, которые были добавлены ранее



## Получить сервис из локатора:
```
ServiceLocator locator;
Service service = locator.Get<IServiceInterface>();
```



## Создать гарантированный запрос на получение сервиса из локатора (если сервис еще не добавлен, то будем ждать пока он не добавится):
```
ServiceLocator locator;
Service service;
locator.Get<IServiceInterface>(guarateedAction: result => service = result);
```



## Если сервисов несолько, и нужно получить конкретный:
```
ServiceLocator locator;
int TargetId = 777;
Service service = locator.Get<IServiceInterface>(condition: c => c.Id == TargetId);
```

## МОДУЛИ

Модули нужны для управления конкретными объектами, пока они находятся в локаторе.

Модули можно динамически добавлять и удалять. 
```
ServiceLocator locator;
IServiceLocatorModule module;
locator.Modules.Add(module);
locator.Modules.Remove(module);
```

Но следует помнить, что при удалении модуля, объеты перестанут выолнять часть функциона, связанного с реализацией интерфейса, подконтрольного модулю.

Модули, добавленные в локатор, только обрабатывают объекты, подконтрольные локатору.

Все управление именно модулями осуществляется ВНЕ локатора, и реализация остается за разроботчиком.

### UpdateServiceLocatorModule
Обрабатывает объекты реализующие интерфейс IUpdatedService.

Используется для вызова метода Update(deltaTime).

### PauseServiceLocatorModule
Обрабатывает объекты реализующие интерфейс IPauseAddictedService.

Используется для перехвата вызова OnApplicationPause.


### DisposableServiceLocatorModule
Обрабатывает объекты реализующие интерфейс IDisposable.

Используется для финализации объекта при его удалении из локатора
